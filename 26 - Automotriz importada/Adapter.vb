﻿Public MustInherit Class AdapterVehiculoImportado
    Inherits VehiculoNacionalizado

    Private _importado As New VehiculoImportado

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Console.WriteLine("convirtiendo km en millas")
            Return _importado.DistanciaRecorridaEnMillas * 1.609
        End Get
        Set(value As Double)
            Console.WriteLine("convirtiendo millas en km")
            _importado.DistanciaRecorridaEnMillas = value / 1.609
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Return _importado.PresionPSI * 0.069
        End Get
        Set(value As Double)
            _importado.PresionPSI = value / 0.069
        End Set
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Return _importado.VelocidadEnMillas * 1.609
        End Get
        Set(value As Double)
            _importado.VelocidadEnMillas = value / 1.609
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Return _importado.VolumenGalon * 3.78
        End Get
        Set(value As Double)
            _importado.VolumenGalon = value / 3.78
        End Set
    End Property
End Class


Public Class VehiculoImportado
    Public Property VelocidadEnMillas As Double
    Public Property DistanciaRecorridaEnMillas As Double
    Public Property VolumenGalon As Double
    Public Property PresionPSI As Double
End Class


Public Class CamionetaImportada
    Inherits AdapterVehiculoImportado

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return 150000 * 15
        End Get
    End Property
End Class

Public Class AutoImportado
    Inherits AdapterVehiculoImportado

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return 100000 * 15
        End Get
    End Property
End Class