﻿Public MustInherit Class Decorator
    Inherits VehiculoNacionalizado


    Protected _vehiculo As VehiculoBase



End Class


Public Class Software
    Inherits Decorator

    Public Sub New(vehiculo As VehiculoBase)
        _vehiculo = vehiculo

    End Sub

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return _vehiculo.Valor + (1500 * 15)
        End Get
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property
End Class

Public Class Alarma
    Inherits Decorator

    Public Sub New(vehiculo As VehiculoBase)
        _vehiculo = vehiculo

    End Sub

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return _vehiculo.Valor + 5000
        End Get
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property
End Class

Public Class Polarizado
    Inherits Decorator

    Public Sub New(vehiculo As VehiculoBase)
        _vehiculo = vehiculo

    End Sub

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return _vehiculo.Valor + 1500
        End Get
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property
End Class

Public Class Baliza
    Inherits Decorator

    Public Sub New(vehiculo As VehiculoBase)
        _vehiculo = vehiculo

    End Sub

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return _vehiculo.Valor + 1000
        End Get
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property
End Class

Public Class Neon
    Inherits Decorator


    Public Sub New(vehiculo As VehiculoBase)
        _vehiculo = vehiculo

    End Sub

    Public Overrides Property DistanciaRecorridaEnKm As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property PresionBAR As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides ReadOnly Property Valor As Double
        Get
            Return _vehiculo.Valor + 5000
        End Get
    End Property

    Public Overrides Property VelocidadEnKH As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property

    Public Overrides Property VolumenLitros As Double
        Get
            Throw New NotImplementedException()
        End Get
        Set(value As Double)
            Throw New NotImplementedException()
        End Set
    End Property
End Class