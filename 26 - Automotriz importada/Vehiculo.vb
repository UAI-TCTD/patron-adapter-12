﻿
Public Interface VehiculoBase

    ReadOnly Property Valor As Double

End Interface

Public MustInherit Class VehiculoNacionalizado
    Implements VehiculoBase

    Protected _velocidad As Double
    Protected _distancia As Double
    Protected _volumen As Double
    Protected _presion As Double

    Public MustOverride Property VelocidadEnKH As Double
    Public MustOverride Property DistanciaRecorridaEnKm As Double
    Public MustOverride Property VolumenLitros As Double
    Public MustOverride Property PresionBAR As Double

    Public MustOverride ReadOnly Property Valor As Double Implements VehiculoBase.Valor

End Class